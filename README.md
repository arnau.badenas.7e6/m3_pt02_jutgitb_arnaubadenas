# Jutge ITB

Jutge ITB es un programa que permet als usuaris crear els seus propis programes en el llenguatge de programació que prefereixin
per solventar diferents enigmes.

## Instal·lació

Descarregar programa d'[aquest repositori](https://gitlab.com/arnau.badenas.7e6/m3_pt02_jutgitb_arnaubadenas).

## Ús
Com es pot veure en aquest exemple, pots decidir si vols solventar el problema o no. Si vols, escriu Si, si no, el programa passará al següent problema. També mostra un exemple del problema perquè sàpigues com sería el resultat d'un exemple.


```text

     ██ ██    ██ ████████  ██████  ███████     ██ ████████ ██████  
     ██ ██    ██    ██    ██       ██          ██    ██    ██   ██ 
     ██ ██    ██    ██    ██   ███ █████       ██    ██    ██████  
██   ██ ██    ██    ██    ██    ██ ██          ██    ██    ██   ██ 
 █████   ██████     ██     ██████  ███████     ██    ██    ██████  
                                                                                                                                        
------Problema 1--------
 Donada una string, escriu un programa que retorni aquesta string en format de array
--- Exemple ---
 Input: Hola
 Output: ['H','o','l','a']
Vols resoldre aquest problema? (Si/No): 
```
No et preocupis! Al final del programa et tornarà a preguntar si vols intentar resoldre els problemes encara no fets o no solventats.

Seguidament, el programa dona una entrada i a partir d'aquesta, haurás de fer un programa en el teu llenguatge preferit que pugui retornar la resposta, i escriure-la al apartat requerit.
```text
Donada la entrada, resol el problema mencionat anteriorment:
Input: Supercalifragilisticoespialidoso
Introdueix la resposta: 
```
I així es repeteix fins que l'usuari es cansi. Si no s'encerta, permet al usuari intentar-ho de nou
```text
Donada la entrada, resol el problema mencionat anteriorment:
Input: Supercalifragilisticoespialidoso
Introdueix la resposta: 
No es correcte...
Vols intentar-ho de nou? (Si/No): 
```
I per no perdre els intents fets anteriorment, quan l'usuari torna al problema a la següent iteració o al següent intent, mostra els intents fets!

```text
Donada la entrada, resol el problema mencionat anteriorment:
Input: 3888
Introdueix la resposta: 1234
No es correcte...
Vols intentar-ho de nou? (Si/No): Si
Has intentat aquest problema 1 vegades.
De moment has intentat: [1234]

Donada la entrada, resol el problema mencionat anteriorment:
Input: 3888
Introdueix la resposta: 
```
## Contribució

No es permet contribuir ni copiar el programa.

## Llicència d ús

[NO ESTA PERMÉS](https://i.scdn.co/image/ab67616d0000b273b89b03329f569ba9860fe3bb)
