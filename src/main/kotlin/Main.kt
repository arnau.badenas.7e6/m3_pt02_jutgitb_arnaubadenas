/**
 * Problem
 *
 * @property statement Represents the statement of the current problem
 * @property publicInput Example input
 * @property publicOutput Example output
 * @property privateInput Private input sent to user
 * @property privateOutput Private Output hidden from user
 * @property result Boolean, If the user solves the problem. Defaulted to false.
 * @property attempts Number of attempts of this problem. Defaulted to 0.
 * @property currentAttempts Attempts done by the user, saved on a list. Defaulted to mutableList so it can be filled
 * as the user tries to solve the problem.
 *
 * @constructor Creates an empty problem object.
 * @author Arnau Badenas
 * @version 1.0
 */
class Problem(
    private val statement: String,
    private val publicInput: String,
    private val publicOutput: String,
    private val privateInput: String,
    private val privateOutput: String,
    var result: Boolean = false,
    var attempts: Int = 0,
    var currentAttempts: MutableList<String> = mutableListOf()
) {
    /**
     * Display statement -> Displays statement of the current problem in a readable format
     *
     * @param id Number to assign to the current problem
     * @author Arnau Badenas
     * @version 1.0
     */
    fun displayStatement(id: Int) {
        val text = """
            ------Problema $id--------
             $statement
            --- Exemple ---
             Input: $publicInput
             Output: $publicOutput
        """.trimIndent()
        println(text)
    }

    /**
     * Display private input -> Displays private input in a readable format.
     *
     * @author Arnau Badenas
     * @version 1.0
     */
    fun displayPrivateInput() {
        val text = """
            
             Donada la entrada, resol el problema mencionat anteriorment:
             Input: $privateInput
        """.trimIndent()
        println(text)
    }

    /**
     * Solve problem -> Checks if the input sent by the user is the same as the one specified in the program's private output.
     *
     * @param answer
     * @return
     * @author Arnau Badenas
     * @version 1.0
     */
    fun solveProblem(answer: String): Boolean {
        if (this.privateOutput == answer) this.result = true
        return result
    }

    /**
     * Increment attempts -> Increments attempts by 1.
     * @author Arnau Badenas
     * @version 1.0
     */
    fun incrementAttempts() {
        this.attempts += 1
    }

    /**
     * Show attempts -> Shows how many times the user has attempted this program
     * and what those attempts are (in array format.)
     */
    fun showAttempts(){
        if(this.attempts != 0){
            println("Has intentat aquest problema ${this.attempts} vegades.")
            println("De moment has intentat: ${this.currentAttempts}")
        }
    }
}

/**
 * Show welcome menu -> Shows welcome menu in a readable format.
 * @author Arnau Badenas
 * @version 1.0
 */
fun showWelcomeMenu() {
    val welcomeText = """ 

     ██ ██    ██ ████████  ██████  ███████     ██ ████████ ██████  
     ██ ██    ██    ██    ██       ██          ██    ██    ██   ██ 
     ██ ██    ██    ██    ██   ███ █████       ██    ██    ██████  
██   ██ ██    ██    ██    ██    ██ ██          ██    ██    ██   ██ 
 █████   ██████     ██     ██████  ███████     ██    ██    ██████  
                                                                                                                                        
    """.trimIndent()
    println(welcomeText)
}


/**
 * Main -> Main is the entry point for the program. It's mandatory in kotlin!
 * @author Arnau Badenas
 * @version 1.0
 */
fun main() {

    //Questions
    val problem1 = Problem(
        "Donada una string, escriu un programa que retorni aquesta string en format de array",
        "Hola",
        "['H','o','l','a']",
        "Supercalifragilisticoespialidoso",
        "['S','u','p','e','r','c','a','l','i','f','r','i','s','t','i','c','o','e','s','p','i','a','l','i','d','o','s','o']"
    )
    val problem2 = Problem(
        "Escriu un programa que llegeixi un nombre enter i imprimeixi una frase amb el següent nombre enter.",
        "5",
        "Després ve el 6",
        "23",
        "Després ve el 24"
    )
    val problem3 = Problem(
        "Llegeix el diàmetre d'una pizza rodona i imprimeix la seva superfície. Pots usar Math.PI per escriure el valor de Pi.",
        "38",
        "1133.54",
        "55",
        "2375.83"
    )
    val problem4 = Problem(
        "Fes un programa que calculi l'equivalent en peus una longitud en metres.",
        "30",
        "98.4251969",
        "515",
        "1689.63255"
    )
    val problem5 = Problem(
        "Fes un programa que calculi quants minuts, hores i segons hi ha en un número de segons donats.",
        "4500",
        "1 hora 15 minuts 0 segons",
        "3888",
        "1 hora 4 minuts 48 segons"
    )

    val problems = mutableListOf<Problem>(problem1, problem2, problem3, problem4, problem5)

    //Game
    showWelcomeMenu()
        do {
            problems.forEachIndexed { i, problem ->
                problem.displayStatement(i + 1)
                print("Vols resoldre aquest problema? (Si/No): ")
                if (readln() == "Si") {
                    do {
                        //Show and solve problem
                        problem.showAttempts()
                        problem.displayPrivateInput()
                        print("Introdueix la resposta: ")
                        val answer = readln()

                        //tryAgain iniciated by default in "No" so the player doesn't try again if the question is correct.
                        var tryProblemAgain = "No"

                        //Problem check
                        if (problem.solveProblem(answer)) {
                            println("Problema resolt!!\n\n")
                            problems.remove(problem)
                        } else {
                            problem.currentAttempts.add(answer)
                            println("No es correcte...")
                            print("Vols intentar-ho de nou? (Si/No): ")
                            tryProblemAgain = readln()
                            if (tryProblemAgain == "Si") problem.incrementAttempts()
                        }
                    } while (tryProblemAgain == "Si")
                }
            }
            var retryGame = "no"
            if(problems.all { problem ->  !problem.result}){
                println("Vols tornar a intentar els intents no resolts?")
                retryGame = readln()
            }
        }while (retryGame == "si")
        println("Esperem que hagis gaudit de la experiència!" +
                "\n Sortint...")

    }