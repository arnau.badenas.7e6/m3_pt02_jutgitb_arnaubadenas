import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class ItbJutgeTest {
    //Sample problem test
    val problemTest = Problem("Suma dos numeros","2 2","4","3 3","9")
    //solveProblem
    @Test
    fun checkSolveProblemAnswerResultIsWrong() {
        val wrong = problemTest.solveProblem("3")
        assertFalse(wrong)
    }
    @Test
    fun checkSolveProblemAnswerResultIsRight(){
        val right = problemTest.solveProblem("9")
        assertTrue(right)
    }

    //incrementAttempts
    @Test
    fun checkAttemptsIncreasedBy1() {
        val expected = problemTest.attempts+1
        problemTest.incrementAttempts()
        val actual = problemTest.attempts
        assertEquals(expected,actual)
    }

    @Test
    fun checkAttemptsIncreasedByMoreThan1() {
        val illegal = problemTest.attempts+2
        problemTest.incrementAttempts()
        val actual = problemTest.attempts
        assertNotEquals(illegal,actual)
    }
}